<!-- Begin Cloud -->
	<section class="cloud" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'cloud' ); ?>
			</div>
		</div>
	</section>
<!-- End Cloud -->