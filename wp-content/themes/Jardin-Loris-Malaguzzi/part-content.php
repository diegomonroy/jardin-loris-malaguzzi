<?php get_template_part( 'part', 'banner' ); ?>
<?php
$style = '';
if ( is_front_page() ) : $style = 'home'; endif;
?>
<?php if ( is_front_page() ) : get_template_part( 'part', 'block-0' ); endif; ?>
<!-- Begin Content -->
	<section class="content <?php echo $style; ?>" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->